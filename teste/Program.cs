﻿using core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace teste
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Montando venda..");
            Venda objVenda = new Venda();
            objVenda.id = Guid.NewGuid();

            //dados do cliente
            objVenda.cliente = new Cliente();
            objVenda.cliente.id = Guid.NewGuid();
            objVenda.cliente.nome = "Jose Maria da Silva";
            objVenda.cliente.cpf = "35281474588";
            objVenda.cliente.endereco = "Rua de teste";
            objVenda.cliente.numero = "123";
            objVenda.cliente.complemento = "Apt. 101";
            objVenda.cliente.bairro = "Centro";
            objVenda.cliente.cidade = "Belo Horizonte";
            objVenda.cliente.estado = "MG";
            objVenda.cliente.cep = "31320240";
            objVenda.cliente.telefone = "31999998888";
            objVenda.cliente.email = "test@test.com";

            //produtos
            objVenda.itens = new List<ItemVenda>();
            objVenda.itens.Add(new ItemVenda()
            {
                id = Guid.NewGuid(),
                produto = new Produto()
                {
                    codigo = "5051",
                    nome = "Camisa Polo",
                    valor = 50
                },
                quantidade = 2
            });
            objVenda.itens.Add(new ItemVenda()
            {
                id = Guid.NewGuid(),
                produto = new Produto()
                {
                    codigo = "5053",
                    nome = "Oculos Feminino",
                    valor = 90
                },
                quantidade = 1
            });

            objVenda.data = DateTime.UtcNow;

            Console.WriteLine("Objeto de venda montado.");
            Console.WriteLine("Criando NF no Notazz...");
            core.Notazz.Client client = new core.Notazz.Client();

            //Altere para sua chave
            client.APIKey = "SUA CHAVE";


            try
            {
                Console.WriteLine("NF criada - ID:" + client.CriarNFProduto(objVenda));

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
