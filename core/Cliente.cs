﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core
{
    public class Cliente
    {
        public Guid id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public DateTime datanascimento { get; set; }
        public string cpf { get; set; }
        public string telefone { get; set; }
        public string cep { get; set; }
        public string endereco { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
    }
}
