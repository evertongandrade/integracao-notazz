﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core
{
    public class Produto
    {
        public string codigo { get; set; }
        public string nome { get; set; }
        public decimal valor { get; set; }
    }
}
