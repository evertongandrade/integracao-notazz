﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Notazz
{
    public class Retorno
    {
        public string statusProcessamento { get; set; }
        public string id { get; set; }
        public string motivo { get; set; }
        public string numero { get; set; }
        public string chave { get; set; }
        public string pdf { get; set; }
        public string xml { get; set; }
        public string xmlCancelamento { get; set; }
        public string emissao { get; set; }
        public string statusNota { get; set; }
        public string rastreio { get; set; }
    }
}
