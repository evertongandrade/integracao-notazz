﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Notazz
{
    public class Produto
    {
        public string DOCUMENT_PRODUCT_COD { get; set; }
        public string DOCUMENT_PRODUCT_NAME { get; set; }
        public int DOCUMENT_PRODUCT_QTD { get; set; }
        public decimal DOCUMENT_PRODUCT_UNITARY_VALUE { get; set; }
        public decimal DOCUMENT_PRODUCT_DISCOUNT { get; set; }
    }
}
