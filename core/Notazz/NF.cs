﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.Notazz
{
    public class NF
    {
        public NF(Venda objVenda)
        {
            this.DESTINATION_NAME = objVenda.cliente.nome;
            this.DESTINATION_TAXID = objVenda.cliente.cpf;
            this.DESTINATION_TAXTYPE = "F"; 
            this.DESTINATION_STREET = objVenda.cliente.endereco;
            this.DESTINATION_NUMBER = objVenda.cliente.numero;
            this.DESTINATION_COMPLEMENT = objVenda.cliente.complemento;
            this.DESTINATION_DISTRICT = objVenda.cliente.bairro;
            this.DESTINATION_CITY = objVenda.cliente.cidade;
            this.DESTINATION_UF = objVenda.cliente.estado;
            this.DESTINATION_ZIPCODE = objVenda.cliente.cep;
            this.DESTINATION_EMAIL = objVenda.cliente.email;
            this.DOCUMENT_BASEVALUE = objVenda.valor;
            this.DOCUMENT_PRODUCT = new Dictionary<int, Produto>();
            this.EXTERNAL_ID = Guid.NewGuid().ToString();
            for (int i = 0; i < objVenda.itens.Count; i++)
            {
                this.DOCUMENT_PRODUCT.Add(i, new Produto()
                {
                    DOCUMENT_PRODUCT_COD = objVenda.itens[i].produto.codigo,
                    DOCUMENT_PRODUCT_NAME = objVenda.itens[i].produto.nome,
                    DOCUMENT_PRODUCT_QTD = objVenda.itens[i].quantidade,
                    DOCUMENT_PRODUCT_UNITARY_VALUE = objVenda.itens[i].produto.valor
                });
            }
            
        }

        public string API_KEY
        { get; set; }
        public string METHOD { get; set; }
        public string DESTINATION_NAME { get; set; }
        public string DESTINATION_TAXID { get; set; }
        public string DESTINATION_TAXTYPE { get; set; }
        public string DESTINATION_STREET { get; set; }
        public string DESTINATION_NUMBER { get; set; }
        public string DESTINATION_COMPLEMENT { get; set; }
        public string DESTINATION_DISTRICT { get; set; }
        public string DESTINATION_CITY { get; set; }
        public string DESTINATION_UF { get; set; }
        public string DESTINATION_ZIPCODE { get; set; }
        public string DESTINATION_PHONE { get; set; }
        public string DESTINATION_EMAIL { get; set; }
        public decimal DOCUMENT_BASEVALUE { get; set; }
        public string EXTERNAL_ID { get; set; }
        public Dictionary<int, Produto> DOCUMENT_PRODUCT { get; set; }
    }
}
