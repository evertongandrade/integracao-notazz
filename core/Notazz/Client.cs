﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace core.Notazz
{
    public class Client
    {
        public string APIKey { get; set; }

        public Guid CriarNFProduto(Venda objVenda)
        {
            Guid idNF = Guid.NewGuid();
            core.Notazz.NF modelNF = new core.Notazz.NF(objVenda);
            modelNF.METHOD = "create_nfe_55";
            modelNF.API_KEY = this.APIKey;
            modelNF.EXTERNAL_ID = idNF.ToString();

            string content = JsonConvert.SerializeObject(modelNF);

            NameValueCollection lst = new NameValueCollection();
            lst.Add("fields", content);

            string retorno = string.Empty;

            WebClient objClient = new WebClient();
            objClient.BaseAddress = "https://app.notazz.com/api";
            objClient.Headers.Add("ContentType", "multipart/form-data");

            try
            {
                byte[] resultado = objClient.UploadValues(objClient.BaseAddress, "POST", lst);
                retorno = objClient.Encoding.GetString(resultado);

                Retorno objRetorno = (Retorno)JsonConvert.DeserializeObject(retorno, typeof(Retorno));

                if (objRetorno.statusProcessamento == "erro")
                    throw new Exception(objRetorno.motivo);
                else
                    return idNF;
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    throw new Exception(reader.ReadToEnd());
                }
            }
        }
    }
}
