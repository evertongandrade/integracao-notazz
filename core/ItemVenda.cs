﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core
{
    public class ItemVenda
    {
        public Guid id { get; set; }
        public Produto produto { get; set; }
        public int quantidade { get; set; }

    }
}
