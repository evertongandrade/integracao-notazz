﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core
{
    public class Venda
    {
        public Guid id { get; set; }
        public List<ItemVenda> itens { get; set; }
        public Cliente cliente { get; set; }
        public DateTime data { get; set; }

        public decimal valor
        {
            get
            {
                decimal _valor = 0;

                if (itens != null && itens.Count > 0)
                    for (int i = 0; i < itens.Count; i++)
                    {
                        _valor += itens[i].quantidade * itens[i].produto.valor;
                    }

                return _valor;
            }
        }
    }
}
